/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class IceCream {
  String name;
  String price;
  String image;
  String desc;

  IceCream({this.name, this.price, this.image, this.desc});
}